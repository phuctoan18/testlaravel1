<?php
namespace App\Validates;

use Validator;

class ValidateInterface
{
    protected $rules = [

    ];

    protected $message = [

    ];
    
    public function setValidate($request) {
        $validate = Validator::make(
            $request->all(),
            $this->rules,
            $this->message,
        );
        // $errors = $validate->errors();
        // return $errors->first('email');
        return $validate;
    }
}
