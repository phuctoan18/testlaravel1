<?php 

namespace App\Policies;

use App\models\Admin;

class PostPolicy {

    public function ViewAny(Admin $admin) {
        return $admin->id == 2;
    }

    // public function viewAny(Admin $admin)
    // {
    //     //
    // }

    // /**
    //  * Determine whether the user can view the model.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @param  \App\Models\Admin  $admin
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function view(Admin $user, $blog)
    // {
    //     // return $user->id == auth('admin')->user()->id;
    //     return $blog == 1 ;
    // }

    // /**
    //  * Determine whether the user can create models.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function create(User $user)
    // {
    //     //
    // }

    // /**
    //  * Determine whether the user can update the model.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @param  \App\Models\Admin  $admin
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function update(User $user, Admin $admin)
    // {
    //     //
    // }

    // /**
    //  * Determine whether the user can delete the model.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @param  \App\Models\Admin  $admin
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function delete(User $user, Admin $admin)
    // {
    //     //
    // }

    // /**
    //  * Determine whether the user can restore the model.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @param  \App\Models\Admin  $admin
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function restore(User $user, Admin $admin)
    // {
    //     //
    // }

    // /**
    //  * Determine whether the user can permanently delete the model.
    //  *
    //  * @param  \App\Models\User  $user
    //  * @param  \App\Models\Admin  $admin
    //  * @return \Illuminate\Auth\Access\Response|bool
    //  */
    // public function forceDelete(User $user, Admin $admin)
    // {
    //     //
    // }
}
