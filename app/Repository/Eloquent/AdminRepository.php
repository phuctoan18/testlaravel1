<?php

namespace App\Repository\Eloquent;

use Illuminate\Support\Facades\Hash;

use App\Repository\AdminRepositoryInterface;
use App\Models\Admin;
use App\Policies\AdminPolicy;
use App\Roles\AdminRole;
use Illuminate\Support\Facades\Auth;

class AdminRepository implements AdminRepositoryInterface
{
    protected $adminModel;
    protected $adminPolicy;
    protected $role;

    public function __construct(Admin $admin, AdminPolicy $adminPolicy, AdminRole $role)
    {
        $this->adminModel = $admin;
        $this->adminPolicy = $adminPolicy;
        $this->role = $role;
    }


    public function index()
    {
        // return Admin::role('admin')->get();
        // $user = Admin::permission('view list admin')->get();

        // if (auth('admin')->attempt(['email' => 'admin1@gmail.com', 'password' => 'admin1'])) {
        if ($this->adminPolicy->ViewAny()) {
            return $this->adminModel::all();
        }
        return "bạn không có quyền truy cập";
        // }
        return 'đăng nhập thất bại';
    }


    public function store($data)
    {
        // if (auth('admin')->attempt(['email' => 'admin1@gmail.com', 'password' => 'admin1'])) {
            if ($this->adminPolicy->Create()) {
                return Auth::guard('admin')->user();
                $admin = new Admin();
                $admin->name = $data->name;
                $admin->email = $data->email;
                $admin->description = 'test';
                $admin->password = Hash::make($data->password);
                $admin->image = 'trunghau.png';
                $admin->save();

                $this->role->SetRoleAdmin($admin);
                $this->role->setPermissions($admin);

                return $data;
            }
            return 'Bạn không có quyền';
        // }
    }

    public function edit()
    {

    }

    public function destroy()
    {

    }

    public function login($data)
    {
        if (auth('admin')->attempt(['email' => $data->email, 'password' => $data->password])) {
            return 'login admin thành công';
        }
        return 'login admin thất bại';
    }
}
