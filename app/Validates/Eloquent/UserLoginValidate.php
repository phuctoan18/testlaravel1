<?php
namespace App\Validates\Eloquent;

use Validator;
use App\Validates\ValidateInterface;

class UserLoginValidate extends ValidateInterface
{
    protected $rules = [
        'email' => 'bail|required|email',
        'password' => 'bail|required',
    ];

    protected $message = [
        'password' => ':attribute cannot be left blank',
        'emai' => ':attribute phải email',
    ];
    
}
