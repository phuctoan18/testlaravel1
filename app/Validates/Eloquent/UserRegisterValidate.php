<?php
namespace App\Validates\Eloquent;

use Validator;
use App\Validates\ValidateInterface;

class UserRegisterValidate extends ValidateInterface
{
    protected $rules = [
        'name' => 'bail|required|min:5|max:30',
        'email' => 'bail|required|email',
        'password' => 'bail|required',
        'RefirmPassword' => 'bail|required'

    ];

    protected $message = [
        'name.required' => ':attribute cannot be left blank',
        'name.min' => ':attribute min 5 keyword',
        'name.max' => ':attribute max 30 keyword',
        'password' => ':attribute cannot be left blank',
        'emai' => ':attribute is email',
        'RefirmPassword' => ':attribute not required'
    ];
    
}
