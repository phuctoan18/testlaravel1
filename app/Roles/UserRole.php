<?php

namespace App\Roles;

use App\Models\User;

class UserRole{
    protected $roles = [
        'admin',
    ];
    protected $permissions = [
        'view list admin',
        'delete admin'
    ];

    public function SetRoleUser(User $user) {
        $user->assignRole($this->roles);
    }

    public function setPermissions(User $user) {
        $user->givePermissionTo($this->permissions);
    }
}