<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    protected $redirect = '/demac';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email' => 'bail|required|email',
            'password' => 'bail|required'
        ];
    }

    // public function withValidator($validator)
    // {
    //     $validator->after(function ($validator) {
    //         if($validator->errors('email')) {
    //             return response()->json([
    //                 "status" => "danger",
    //                 "msg" => "errors"
    //             ], 404);
    //         }
    //     });
    // }

    public function messages()
    {
        return [
            'email.required' => 'email not required',
            'password.required' => 'password not required',
        ];
    }
}
