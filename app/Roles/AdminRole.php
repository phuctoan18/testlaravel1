<?php

namespace App\Roles;

use App\Models\Admin;

class AdminRole{
    protected $roles = [
        'admin',
    ];
    protected $permissions = [
        'view list admin',
        'delete admin'
    ];

    public function SetRoleAdmin(Admin $admin) {
        $admin->assignRole($this->roles);
    }

    public function setPermissions(Admin $admin) {
        $admin->givePermissionTo($this->permissions);
    }
}