<?php

namespace App\Repository\Eloquent;

use App\Repository\UserRepositoryInterface;
use App\Models\User;
// use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use App\Roles\UserRole;
use App\Validates\Eloquent\UserLoginValidate;
use App\Validates\Eloquent\UserRegisterValidate;

class UserRepository implements UserRepositoryInterface
{
    // use AuthenticatesUsers;

    protected $userModel;
    protected $role;
    protected $userLoginValidate;
    protected $userRegisterValidate;

    public function __construct(User $user, UserRole $userRole, UserLoginValidate $userLoginValidate, UserRegisterValidate $userRegisterValidate)
    {
        $this->userModel = $user;
        $this->role = $userRole;
        $this->userLoginValidate = $userLoginValidate;
        $this->userRegisterValidate = $userRegisterValidate;;
    }


    public function index()
    {
        $data = $this->userModel::all();
        return response()->json($data, 200);
    }


    public function store($data)
    {
        // $validate = $this->userRegisterValidate->setValidate($data);
        // $errors = $validate->errors();

        // if($errors->any()) {
        //     return response()->json([
        //         "status" => "warning",
        //         "msg" => $errors->all(),
        //     ], 404);
        // }

        $user = new $this->userModel;
        $user->name = $data->name;
        $user->email = $data->email;
        $user->description = 'test';
        $user->password = Hash::make($data->password);
        $user->image = 'trunghau.png';
        $user->save();
        return $data;
    }

    public function edit()
    {
    }

    public function destroy()
    {
    }

    public function login($data)
    {
        $validate = $this->userLoginValidate->setValidate($data);
        $errors = $validate->errors();

        if($errors->any()) {
            return response()->json([
                "status" => "warning",
                "msg" => $errors->all(),
            ], 404);
        }  

        if (auth()->attempt(['email' => $data->email, 'password' => $data->password])) {
            return response()->json('login success', 200);
        }
        return response()->json([
            "status" => "danger",
            "msg" => "Username not found"
        ], 404);
    }
}
