<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\admins as Admin;
use App\http\Controllers\api\user as User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::resource('admin', Admin::class);

// Route::post('admin/login', [Admin::class, 'login'])

// Route::resource('user', User::class);
// Route::post('user/login', [User::class, 'login']);

Route::resource('user', User::class);
Route::post('user/login', [User::class, 'login']);


// Route::group(['middleware' => ['auth:sanctum']], function () {
//     Route::resource('user', User::class);
//     Route::post('user/login', [User::class, 'login']);

// });



