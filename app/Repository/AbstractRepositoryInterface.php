<?php

namespace App\Repository;

interface AbstractRepositoryInterface 
{
    public function index();

    // public function create($data);

    public function store($data);

    public function edit();

    public function destroy();
}

